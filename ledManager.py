from threading import Thread

try:
    import RPi.GPIO as GPIO
except Exception:
    print("RPi.GPIO import fail.")
from time import sleep
import datetime


class Led:
    def __init__(self, RED: int, name: str):
        self.RED = RED
        self.turn0ff_thread = False
        self.name = name
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(self.RED, GPIO.OUT)
        GPIO.output(self.RED, 0)

    def start(self):
        self.turn0ff_thread = False
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(self.RED, GPIO.OUT)
        GPIO.output(self.RED, 0)

    def turnOn_red(self, duration_seconds: int = 0):
        endTime = datetime.datetime.now() + datetime.timedelta(0, duration_seconds)
        self.start()
        while True:
            if duration_seconds > 0:
                if datetime.datetime.now() <= endTime:
                    GPIO.output(self.RED, 1)
                else:
                    self.turn0ff()
                    break
            else:
                if self.turn0ff_thread:
                    self.turn0ff()
                    break
                else:
                    GPIO.output(self.RED, 1)

    def turn0ff(self):
        # self.start()
        GPIO.output(self.RED, 0)
        self.close_GPIO

    def close_GPIO():
        GPIO.cleanup()


def turnOn_thread(
    led: Led,
    blinking=False,
    color: str = "R",
    duration_seconds: int = 0,
    rate: float = 0.5,
):
    turnOff_thread(led)
    if blinking:
        thread = Thread(
            target=led.blinking,
            args=(
                rate,
                color,
                duration_seconds,
            ),
        )
        thread.start()
        # print(f"blinking thread {led.name} start")
    else:
        if color == "R":
            thread = Thread(target=led.turnOn_red, args=(duration_seconds,))
            thread.start()
            print(f"thread {led.name} start")


def turnOff_thread(led: Led):
    led.turn0ff_thread = True


led1 = Led(17, "led1")


## TEST ##
#################################################
print("test Led")
led1.start()
led1.turnOn_red(5)
print("test thread")
turnOn_thread(led1, blinking=True, color="R", duration_seconds=5, rate=0.5)
