#include <Wire.h>
#include <Ultrasonic.h>

#define SLAVE_ADDRESS 11

#define motor1Pin1 2
#define motor1Pin2 3
#define motor2Pin1 4
#define motor2Pin2 5

Ultrasonic Left_ultrasonic(8, 9);
Ultrasonic Front_ultrasonic(10, 11);
Ultrasonic Right_ultrasonic(12, 13);

unsigned short leftDistance;
unsigned short frontDistance;
unsigned short rightDistance;

// Global Vars

char c; // receive char
int x;  // receive int
int targetX = 0;
bool ballDetected = false;

const int BUTTON_PIN = 5; // the number of the pushbutton pin
int compteur = 0;
int countBuffer = 0;

// Variables will change:
int lastState = LOW; // the previous state from the input pin
int currentState;    // the current reading from the input pin

// clean
bool clean = true;
// follow wall
bool followLeft = false;
bool followRight = false;
int followLeftWallNum = 0;
int followRightWallNum = 0;
// cut
bool cutWire = false;
bool yelloWire = false;
bool greenWire = false;
bool blueWire = false;
bool isInCutWireAera = false;
// passwd
bool passwd = false;
bool isInPasswdAera = false;

// push buttons for cut wire
int pushButtonG = 14;
int pushButtonB = 15;
int pushButtonY = 16;
int greenButton = 0;
int blueButton = 0;
int yellowButton = 0;

void setup()
{
    Serial.begin(9600); // open the serial port at 9600 bps:

    // comunications
    Wire.begin(SLAVE_ADDRESS);
    Wire.onReceive(receiveEvent);

    // DC Motors
    pinMode(motor1Pin1, OUTPUT);
    pinMode(motor1Pin2, OUTPUT);
    pinMode(motor2Pin1, OUTPUT);
    pinMode(motor2Pin2, OUTPUT);

    // initialize the pushbutton pin as an pull-up input
    // the pull-up input pin will be HIGH when the switch is open and LOW when the switch is closed.
    pinMode(BUTTON_PIN, INPUT);

    pinMode(pushButtonG, INPUT);
    pinMode(pushButtonB, INPUT);
    pinMode(pushButtonY, INPUT);
}

//------- FUNCTIONS ---------
void receiveEvent(int howMany)
{
    while (1 < Wire.available()) // loop through all but the last
    {
        c = Wire.read(); // receive byte as a character
                         // Serial.print(c); // print the character
    }
    int x = Wire.read(); // receive byte as an integer
    // Serial.println(x);
    //  convert x to int and store in targetX
    if (x == 48)
    {
        targetX = 0;
        ballDetected = false;
    }
    if (x == 49)
    {
        targetX = 1;
        ballDetected = true;
        // if ball detected at left turn
    }
    if (x == 50)
    {
        targetX = 2;
        ballDetected = true;
        // if ball detected at front
    }
    if (x == 51)
    {
        // if ball detected at right turn
        targetX = 3;
        ballDetected = true;
    }
}

void count_ball(int currentState)
{
    if (currentState == HIGH)
    {
        compteur++;
        Serial.println("balls count: " + String(compteur));
    }
    if (compteur == 10)
    {
        clean = false;
        passwd = true;
    }
}

void left_turn(int delayValue)
{
    if (leftDistance > 10 && frontDistance > 10)
    {
        // turn left
        digitalWrite(motor1Pin1, LOW);
        digitalWrite(motor1Pin2, HIGH);
        digitalWrite(motor2Pin1, LOW);
        digitalWrite(motor2Pin2, HIGH);
        delay(delayValue);
        // stop
        digitalWrite(motor1Pin1, LOW);
        digitalWrite(motor1Pin2, LOW);
        digitalWrite(motor2Pin1, LOW);
        digitalWrite(motor2Pin2, LOW);

        // anti collision
        antiCollision();
    }
}
void right_turn(int delayValue)
{
    if (rightDistance > 10 && frontDistance > 10)
    {
        // turn right
        digitalWrite(motor1Pin1, HIGH);
        digitalWrite(motor1Pin2, LOW);
        digitalWrite(motor2Pin1, HIGH);
        digitalWrite(motor2Pin2, LOW);
        delay(delayValue);
        // stop
        digitalWrite(motor1Pin1, LOW);
        digitalWrite(motor1Pin2, LOW);
        digitalWrite(motor2Pin1, LOW);
        digitalWrite(motor2Pin2, LOW);

        // anti collision
        antiCollision();
    }
}
void forward(int distance)
{
    if (frontDistance > distance)
    {
        // forward
        digitalWrite(motor1Pin1, HIGH);
        digitalWrite(motor1Pin2, LOW);
        digitalWrite(motor2Pin1, LOW);
        digitalWrite(motor2Pin2, HIGH);
        delay(distance);
        // stop
        digitalWrite(motor1Pin1, LOW);
        digitalWrite(motor1Pin2, LOW);
        digitalWrite(motor2Pin1, LOW);
        digitalWrite(motor2Pin2, LOW);

        // anti collision
        antiCollision();
    }
    else
    {
        // stop
        digitalWrite(motor1Pin1, LOW);
        digitalWrite(motor1Pin2, LOW);
        digitalWrite(motor2Pin1, LOW);
        digitalWrite(motor2Pin2, LOW);

        // anti collision
        antiCollision();
    }
}
void backward(int distance)
{

    // backward
    digitalWrite(motor1Pin1, LOW);
    digitalWrite(motor1Pin2, HIGH);
    digitalWrite(motor2Pin1, HIGH);
    digitalWrite(motor2Pin2, LOW);
    delay(distance);
    // stop
    digitalWrite(motor1Pin1, LOW);
    digitalWrite(motor1Pin2, LOW);
    digitalWrite(motor2Pin1, LOW);
    digitalWrite(motor2Pin2, LOW);
}
void follow_leftWall(int distance)
{
    // if left wall is to far turn left
    if (leftDistance > 20 && frontDistance > distance)
    {
        left_turn(10);
        forward(10);
        right_turn(10);
        forward(distance);
    }
    // if left wall is to close turn right
    if (leftDistance < 10 && frontDistance > distance)
    {
        right_turn(10);
        forward(10);
        left_turn(10);
        forward(distance);
    }
    // if left wall is in the middle go forward
    if (leftDistance > 10 && leftDistance < 20 && frontDistance > distance)
    {
        forward(distance);
    }
    // if forward wall is to close turn left
    if (frontDistance < 10)
    {
        if (cutWire == true)
        {
            followLeftWallNum += 1;
        }
        right_turn(10);
        forward(distance);
    }
}
void antiCollision()
{
    if (leftDistance < 10 && rightDistance < 10 && frontDistance < 10)
    {
        backward(20);
        left_turn(10);
        forward(10);
        right_turn(10);
        forward(10);
    }
}

void follow_rightWall(int distance)
{
    // if right wall is to far turn right
    if (rightDistance > 20 && frontDistance > distance)
    {
        right_turn(10);
        forward(10);
        left_turn(10);
        forward(distance);
    }
    // if right wall is to close turn left
    if (rightDistance < 10 && frontDistance > distance)
    {
        left_turn(10);
        forward(10);
        right_turn(10);
        forward(distance);
    }
    // if right wall is in the middle go forward
    if (rightDistance > 10 && rightDistance < 20 && frontDistance > distance)
    {
        forward(distance);
    }
    // if forward wall is to close turn right
    if (frontDistance < 10)
    {
        if (cutWire == true)
        {
            followRightWallNum += 1;
        }
        left_turn(10);
        forward(distance);
    }
}
void passwdAeraCheck()
{

    int lateral_constant = 0;
    int front_constant = 0;
    float interLeftRight = 17.5;
    float frontDistanceRequired = 11.5;

    if (leftDistance + rightDistance + lateral_constant == interLeftRight && frontDistance + front_constant == frontDistanceRequired)
    {
        isInPasswdAera = true;
    }
}
void cutWireAeraCheck()
{

    int lateral_constant = 0;
    float interLeftRight = 25;

    if (leftDistance + rightDistance + lateral_constant == interLeftRight)
    {
        isInCutWireAera = true;
    }
}

//------- MAIN ---------
void loop()
{
    // ultraqsonic sensors read
    leftDistance = Left_ultrasonic.read(CM);
    frontDistance = Front_ultrasonic.read(CM);
    rightDistance = Right_ultrasonic.read(CM);
    Serial.println(String(leftDistance) + "___" + String(frontDistance) + "___" + String(rightDistance));
    // if cleean is true
    if (clean == true)
    {
        // if ball detected return value
        if (ballDetected)
        {
            Serial.println("ball detected: " + String(targetX));
            if (targetX == 1)
            {
                // turn left
                left_turn(10);
            }
            if (targetX == 2)
            {
                // go forward
                forward(10);
            }
            if (targetX == 3)
            {
                // turn right
                right_turn(10);
            }
        }
        else
        {
            // follow left wall
            follow_leftWall(10);
        }
        // PushButton read and count
        //----------------------------------------
        currentState = digitalRead(BUTTON_PIN);
        count_ball(currentState);
        //----------------------------------------
    }
    if (passwd == true)
    {
        // if password is true
        // check if is in aera
        if (isInPasswdAera == true)
        {
            // do arm code
            // and set:
            isInPasswdAera = false;
            passwd = false;
            cutWire = true;
        }
        else
        {
            // follow right wall
            follow_rightWall(10);
            // do area check
            passwdAeraCheck();
        }
    }
    if (cutWire == true)
    {
        // if cutWire is true
        // check if is in aera
        if (isInCutWireAera == true)
        {
            if (followLeftWallNum == 2)
            {
                // do arm code
                // read push button
                if (digitalRead(pushButtonG) == 1)
                {
                    if (30 < frontDistance < 46)
                    {
                        // cut green wire code
                    }
                    else
                    {
                        // go forward
                        follow_leftWall(10);
                    }
                }
                if (digitalRead(pushButtonB) == 1)
                {
                    if (56 < frontDistance < 71)
                    {
                        // cut blue wire code
                    }
                    else
                    {
                        // go forward
                        follow_leftWall(10);
                    }
                }
                if (digitalRead(pushButtonY) == 1)
                {
                    if (83 < frontDistance < 99)
                    {
                        // cut red wire code
                    }
                    else
                    {
                        // go forward
                        follow_leftWall(10);
                    }
                }
            }
        }
        else
        {
            // follow left wall
            follow_leftWall(10);
            cutWireAeraCheck();
        }
    }
    if (clean == false && passwd == false && cutWire == false)
    {
        // if all is false
        Serial.println("all is Done");
        left_turn(100);
        // stop
    }
    delay(500);
}