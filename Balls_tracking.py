#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 2023/01/02
# version ='1.0'
# ---------------------------------------------------------------------------
""" Python Balls detection and tracking"""
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
import numpy as np
import cv2
import sys
import smbus2 as smbus  # ,smbus2
import time

ball_coords = ""
cap = cv2.VideoCapture(0)
# Slave Addresses
I2C_SLAVE_ADDRESS = 11  # 0x0b ou 11
I2Cbus = smbus.SMBus(1)

while True:
    success, img = cap.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blurred = cv2.medianBlur(gray, 25)  # cv2.bilateralFilter(gray,10,50,50)

    minDist = 100
    param1 = 30  # 500
    param2 = 50  # 200 #smaller value-> more false circles
    minRadius = 2
    maxRadius = 50  # 10

    # docstring of HoughCircles: HoughCircles(image, method, dp, minDist[, circles[, param1[, param2[, minRadius[, maxRadius]]]]]) -> circles
    circles = cv2.HoughCircles(
        blurred,
        cv2.HOUGH_GRADIENT,
        1.1,
        minDist,
        param1=param1,
        param2=param2,
        minRadius=minRadius,
        maxRadius=maxRadius,
    )

    if circles is not None:
        circles = np.uint16(np.around(circles))
        for i in circles[0, :]:
            cv2.circle(img, (i[0], i[1]), i[2], (0, 255, 0), 2)
            if i[0] < 200:
                ball_coords = f"1"
            if 200 <= i[0] <= 400:
                ball_coords = f"2"
            if 400 < i[0]:
                ball_coords = f"3"
            print(f"x:{i[0]}, y:{i[1]}, r:{i[2]}")
            try:
                with smbus.SMBus(1) as I2Cbus:
                    BytesToSend = ball_coords.encode("utf-8")
                    print(
                        "Sent at: "
                        + str(I2C_SLAVE_ADDRESS)
                        + " the following bytes: "
                        + ball_coords
                    )
                    print(len(BytesToSend))
                    I2Cbus.write_i2c_block_data(I2C_SLAVE_ADDRESS, 0x00, BytesToSend)
                    time.sleep(0.5)
            except:
                print("Communication error with Arduino")

    else:
        ball_coords = f"0"
        try:
            with smbus.SMBus(1) as I2Cbus:
                BytesToSend = ball_coords.encode("utf-8")
                print(
                    "Sent at: "
                    + str(I2C_SLAVE_ADDRESS)
                    + " the following bytes: "
                    + ball_coords
                )
                print(len(BytesToSend))
                I2Cbus.write_i2c_block_data(I2C_SLAVE_ADDRESS, 0x00, BytesToSend)
                time.sleep(0.5)
        except:
            print("Communication error with Arduino")
    # Show result for testing:
    # cv2.imshow("Image", img)
    cv2.waitKey(1)
