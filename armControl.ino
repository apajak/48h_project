#include "Servo.h"
// 8123
Servo servo;  // création de l'objet "servo"
Servo servo1; // création de l'objet "servo"
Servo servo2; // création de l'objet "servo"

int armLength = 90;
int handLength = 172;
int distanceToTarget = 10;

// set target 8 position (x,y,z) at -10,-40,-118
int target1X = ;
int target1Y = -10;
int target1Z = 10;

// set target 2 position (x,y,z) at -10,-25,-98
int target2X = 0;
int target2Y = 10;
int target2Z = 10;

// set target 1 position (x,y,z) at -20,-25,-93
int target2X = 0;
int target2Y = 10;
int target2Z = 10;

// set target 3 position (x,y,z) at +20,-25,-93
int target2X = 0;
int target2Y = 10;
int target2Z = 10;




void setup()
{
    servo.attach(4);  // attache le servo au pin spécifié
    servo1.attach(5); // attache le servo au pin spécifié
    servo2.attach(6); // attache le servo au pin spécifié
}

void loop()
{
    servo.write(0);  // demande au servo de se déplacer à cette position
    servo1.write(0); // demande au servo de se déplacer à cette position
    servo2.write(0); // demande au servo de se déplacer à cette position
    delay(1000);     // attend 1000 ms entre changement de position

    servo.write(90);  // demande au servo de se déplacer à cette position
    servo1.write(90); // demande au servo de se déplacer à cette position
    servo2.write(90); // demande au servo de se déplacer à cette position
    delay(1000);      // attend 1000 ms entre changement de position

    servo.write(180);  // demande au servo de se déplacer à cette position
    servo1.write(180); // demande au servo de se déplacer à cette position
    servo2.write(180); // demande au servo de se déplacer à cette position
    delay(1000);       // attend 1000 ms entre changement de position
}